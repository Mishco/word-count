import sys

import my_count
from my_count import main

if __name__ == "__main__":
    my_count.INFINITE_LOOP = False
    main(sys.argv[1:])
