"""
Word count
author: Michal Slovik
https://ccd-school.de/coding-dojo/agility-katas/word-count-i/
"""
import getopt
import os
import re
import sys
from collections import Counter

WORD_PATTERN = "[a-z-A-Z]*"
INFINITE_LOOP = True


def simple_word_count(input_value_text, index=False):
    """
        Write an application to count the number of words in a text.
        The app will ask the user for the text upon start.
        It will then output the number of words found in the text.
        Words are stretches of letters (a-z,A-Z).

        Not all words are relevant for counting.
        A list of stop words (provided in the file „stopwords.txt“)
        defines which words not to count.

        :param index: if you want to return index of selected words
        :arg input_value_text string of full text
        :return count (integer) of words number
    """

    lines = re.findall(WORD_PATTERN, input_value_text)
    count = 0
    avg_len = 0

    script_dir = os.path.dirname(__file__)
    rel_path = "stopwords.txt"
    abs_file_path = os.path.join(script_dir, rel_path)

    with open(abs_file_path, encoding="UTF-8", mode="r") as file_handle:
        stop_words = file_handle.readlines()
        stop_words = [x.strip() for x in stop_words]

    selected_words = []
    for word in lines:
        if len(word) > 1 and re.match(WORD_PATTERN, word).endpos > 0:
            if word not in stop_words:
                count += 1
                selected_words.append(word)
                avg_len += len(word)

    unique_count = len(Counter(selected_words).items())
    result_avg = 0
    if count != 0:
        result_avg = avg_len / count

    if index:
        return count, unique_count, result_avg, sorted(selected_words, key=str.lower)

    return count, unique_count, result_avg, None


def work_with_stdio(index=False):
    """
    Count the number of words from standard input
    :return: count of words
    """
    while True:
        input_text = input("Enter text: ")

        if input_text:
            if index:
                count_words, count_unique, avg_len, index_words = \
                    simple_word_count(input_text, index)
                print("Number of words: {}, unique: {}; "
                      "average word length: {:.2f} characters\nIndex:".
                      format(count_words, count_unique, avg_len))
                print(*index_words, sep='\n')
            else:
                count_words, count_unique, avg_len, _ = \
                    simple_word_count(input_text, index)
                print("Number of words: {}, unique: {}; "
                      "average word length: {:.2f} characters".
                      format(count_words, count_unique, avg_len))
        else:
            sys.exit(0)
        if INFINITE_LOOP:
            return


def work_with_file_as_input(arg):
    """
    Count the number of words from text file
    :param arg: name of temporary file
    :return: count of words
    """
    input_file = arg
    with open(input_file, encoding="UTF-8", mode="r") as file_handle:
        whole_lines = file_handle.readlines()
        string_whole_lines = " ".join(map(str, whole_lines))

    count_words, count_unique, avg_len, _ = simple_word_count(string_whole_lines, index=False)
    print("Number of words: {}, unique: {}; average word length: {:.2f} characters".
          format(count_words, count_unique, avg_len))


def work_with_dict(index=True, dictionary='dict.txt'):
    """
    Count the number of words
    :param index: true/false if you want to print selected words
    :param dictionary: file to use as dictionary
    :return: only print
    """
    input_text = input("Enter text: ")
    count_words, count_unique, avg_len, index_words = simple_word_count(input_text, index=index)
    unknown = 0

    with open(dictionary, encoding="UTF-8", mode="r") as file_handle:
        whole_lines = file_handle.readlines()
        string_whole_lines = " ".join(map(str, whole_lines))

    for idx, _ in enumerate(index_words):
        if index_words[idx] not in string_whole_lines:
            unknown += 1
            index_words[idx] = index_words[idx] + "*"

    print("Number of words: {}, unique: {}; average word length: {:.2f} characters\n"
          "Index (unknown: {}):".
          format(count_words, count_unique, avg_len, unknown))
    print(*index_words, sep='\n')


def stdio_workflow(args):
    """
    Simple workflow with stdio or file
    :param args: file name, if not, wait for input
    :return: word count
    """
    if args:
        for arg in args:
            work_with_file_as_input(arg)
    else:
        work_with_stdio(index=False)


def consume_args_opts(opts):
    """
    Process all opts and decide what should be run next
    :param opts: input params
    :return: Boolean, Filename, Boolean
    """
    index_config = False
    dict_config = False
    dict_value = None
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            write_help()
            sys.exit(0)
        if opt in ('-i', '--index'):
            index_config = True
        if opt in ('-d', '--dictionary'):
            dict_config = True
            dict_value = arg
    return dict_config, dict_value, index_config


def get_opts_args(argv):
    """
    Get opts and args from input params
    :param argv: input params
    :return: args, opts
    """
    try:
        opts, args = getopt.getopt(argv, shortopts="hd:", longopts=["help", "index", "dictionary="])
    except getopt.GetoptError:
        write_help()
        sys.exit(2)
    return args, opts


def main(argv):
    """
    Count of words in file or from stdin
    :param argv: if presented it should be filename
    :return: count of words
    """
    args, opts = get_opts_args(argv)

    dict_config, dict_value, index_config = consume_args_opts(opts)

    if index_config and dict_config:
        work_with_dict(index=index_config, dictionary=dict_value)
        return
    if index_config and not dict_config:
        work_with_stdio(index=True)
        return

    stdio_workflow(args)


def write_help():
    """
    Show help for this command tool
    """
    print("wordcount.py --index --dictionary='dict.txt'")
