import functools
from threading import Thread


def timeout(timeout):
    """
    Timeout helper.
    Windows alternative for https://pypi.org/project/timeout-decorator/
    :param timeout: how long should wait
    :return: Exception if time is out.
    """

    def deco(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            res = \
                [Exception('function [%s] timeout [%s seconds] exceeded!'
                           % (func.__name__, timeout))]

            def new_func():
                try:
                    res[0] = func(*args, **kwargs)
                except Exception as e:
                    res[0] = e

            t = Thread(target=new_func)
            t.daemon = True
            try:
                t.start()
                t.join(timeout)
            except Exception as je:
                print('error starting thread')
                raise je
            ret = res[0]
            if isinstance(ret, BaseException):
                raise ret
            return ret

        return wrapper

    return deco
