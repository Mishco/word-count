# Word count

[![pipeline status](https://gitlab.com/Mishco/word-count/badges/master/pipeline.svg)](https://gitlab.com/Mishco/word-count/-/commits/master)
[![coverage report](https://gitlab.com/Mishco/word-count/badges/master/coverage.svg)](https://gitlab.com/Mishco/word-count/-/commits/master)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=bugs)](https://sonarcloud.io/summary/new_code?id=word-count)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=word-count)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=word-count)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=word-count)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=word-count)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=word-count&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=word-count)

Inspired by: https://ccd-school.de/coding-dojo/#cd8.

[toc]

## Requirements and Usage

```
$ python --version
Python 3.9.7
```

```cmd
$ python .\wordcount.py
Enter text: Mary had a little lamb
Number of words: 4
$ 
```

Or count in file

```cmd
python .\wordcount.py mytext.txt
Number of words: 4
```

## Tests

Test by `flake8`, `bandit` and `black`:

```cmd
flake8 .\test_word_count.py
bandit -r my_count
black .\test_word_count.py  
```

## Stages

### 1 - Agility Kata „Word Count I“

Write an application to count the number of words in a text. The app will ask the user for the text upon start. It will
then output the number of words found in the text. Words are stretches of letters (a-z,A-Z). Sample usage:

```cmd
$ wordcount
Enter text: Mary had a little lamb
Number of words: 5
```

### 2 - Agility Kata „Word Count II“

Not all words are relevant for counting. A list of stop words (provided in the file „stopwords.txt“)
defines which words not to count. Example of stopwords.txt with each line containing a stop word:

```
the
a
on
off
```

Usage:

```cmd
$ wordcount
Enter text: Mary had a little lamb
Number of words: 4
```

### 3 - Agility Kata „Word Count III“

The application can be started with a filename to read the text from instead of entering it manually. If no filename is
provided, the application will still ask for a text. Sample usage:

```cmd
$ wordcount mytext.txt
Number of words: 4
```

With mytext.txt being:

```
Mary had
a little
lamb
```

### 4 - Agility Kata „Word Count IV“

The application not only shows the number of words, but also the number of unique words. Sample usage:

```
$ wordcount
Enter text: Humpty-Dumpty sat on a wall. Humpty-Dumpty had a great fall.
Number of words: 9, unique: 7
```

### 5 - Agility Kata "Word Count V"

Words containing a hyphen are no longer separated, e.g. „Humpty-Dumpty“ will be considered 1 word instead of 2.

```
$ wordcount
Enter text: Humpty-Dumpty sat on a wall. Humpty-Dumpty had a great fall.
Number of words: 7, unique: 6
$
```

### 6 - Agility Kata „Word Count VI“

The average word length of counted words is calculated and output, e.g.

```
$ wordcount sometext.txt
Number of words: 14, unique: 10; average word length: 5.63 characters
```

### 7 - Agility Kata "Word Count VII"

Optionally an index of all counted words is printed. Sample usage:

```
$ wordcount -index
Enter text: Mary had a little lamb
Number of words: 4, unique: 4; average word length: 4.25 characters
Index:
had
lamb
little
Mary
$
```

### 8 - Agility Kata „Word Count VIII“

Optionally the text can be checked against a dictionary of known words. If the index is printed it will mark words not
found in the dictionary with a star and state the number of unknown words, e.g.

```
$ wordcount -index -dictionary=dict.txt
Enter text: Mary had a little lamb
Number of words: 4, unique: 4; average word length: 4.25 characters
Index (unknown: 2):
had
lamb*
little
Mary*
$
```

With _dict.txt_ being:

```
big
small
little
cat
dog
have
has
had
```

### 9 - Agility Kata „Word Count IX“

Allow the user to enter several texts and get them analyzed. The program is terminated by entering an empty text:

```
$ wordcount
Enter text: Mary had a little lamb
Number of words: 4, unique: 4; average word length: 4.25 characters

Enter text: a bb ccc dddd
Number of words: 4, unique: 4; average word length: 2.5 characters

Enter text:
$
```
