import io
import sys
from unittest import TestCase
from unittest.mock import patch

import my_count
import wordcount
from my_count import simple_word_count
from my_count.common import timeout

HAD_LAMB_LITTLE_MARY_EXCEPTED_OUT = "Number of words: 4, unique: 4; " \
                                    "average word length:" \
                                    " 4.25 characters\n" \
                                    "Index:\n" \
                                    "had\n" \
                                    "lamb\n" \
                                    "little\n" \
                                    "Mary"

SAMPLE_TEXT = "Mary had a little lamb"

SYS_STDOUT = 'sys.stdout'

BUILTINS_INPUT = 'builtins.input'


class Test(TestCase):
    def test_sample_words(self):
        input_txt = SAMPLE_TEXT
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(4, result)

    def test_empty_word(self):
        input_txt = " "
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(0, result)

    def test_non_alpha_words(self):
        input_txt = "asdasda 1325"
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(1, result)

    def test_number_alpha_words(self):
        input_txt = "123 1325"
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(0, result)

    def test_invalid_words(self):
        input_txt = ")%@#!@@#!$& (%%^"
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(0, result)

    def test_long_words_not_separated(self):
        input_txt = "maryhasalambbutitisnotseparatedsoitshouldbeonlyone"
        result, _, _, _ = simple_word_count(input_txt)
        self.assertEqual(1, result)

    def test_stop_words(self):
        input_txt = SAMPLE_TEXT
        result, unique, avg_len, _ = simple_word_count(input_txt)
        self.assertEqual(4, result)
        self.assertEqual(4, unique)
        self.assertEqual(4.25, avg_len)

    def test_all_stop_words(self):
        with open("my_count/stopwords.txt", "r") as file_handle:
            stop_words = file_handle.readlines()
            stop_words = [x.strip() for x in stop_words]

        for stop_word in stop_words:
            result, _, _, _ = simple_word_count(stop_word)
            self.assertEqual(0, result)

    @patch('sys.argv', ['my_count/__init__.py', 'my_count/mytext.txt'])
    def test_main_loop(self):
        args = sys.argv[1:]
        captured_output = io.StringIO()
        sys.stdout = captured_output
        my_count.main(args)
        sys.stdout = sys.__stdout__
        self.assertEqual("Number of words: 4, unique: 4;"
                         " average word length: 4.25 characters\n",
                         captured_output.getvalue())

    def runTest(self, given_answer, expected_out, args):
        with patch(BUILTINS_INPUT, return_value=given_answer), \
                patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
            my_count.main(args)
            self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    def test_args_input(self):
        self.runTest('test a word', 'Number of words: 2, unique: 2;'
                                    ' average word length: 4.00 characters',
                     [])

    def test_empty_line_args_input(self):
        self.runTest(' ', 'Number of words: 0, unique: 0;'
                          ' average word length: 0.00 characters', [])

    def test_empty_line_and_help_args_input(self):
        with self.assertRaises(SystemExit):
            self.runTest(' ', "wordcount.py --index --dictionary='dict.txt'",
                         ['--some strange args'])

    def helper_call(self):
        with self.assertRaises(SystemExit):
            args = sys.argv[1:]
            my_count.main(args)

    @patch('sys.argv', ['my_count/__init__.py', '-h'])
    def test_call_help(self):
        self.helper_call()

    @patch('sys.argv', ['my_count/__init__.py', '--help'])
    def test_call_another_help(self):
        self.helper_call()

    @patch('sys.argv', ['my_count/__init__.py', '--g'])
    def test_not_valid_agr(self):
        self.helper_call()

    def test_count_unique_word(self):
        # Old (Humpty-Dumpty=2 words) Number of words: 9, unique: 7
        # New (Humpty-Dumpty=1 word) Number of words: 7, unique: 6
        input_text = "Humpty-Dumpty sat on a wall. " \
                     "Humpty-Dumpty had a great fall."
        number_of_words, unique, _, _ = simple_word_count(input_text)

        self.assertEqual(7, number_of_words)
        self.assertEqual(6, unique)

    def test_fix_non_valid_word(self):
        # Enter text: -
        # Number of words: 1, unique: 1 and should be 0
        self.runTest('-', 'Number of words: 0, unique: 0;'
                          ' average word length: 0.00 characters', [])

    @patch('sys.argv', ['my_count/__init__.py', 'my_count/sometext.txt'])
    def test_avg_len_loop(self):
        # Number of words: 14, unique: 10;
        # average word length: 5.63 characters
        args = sys.argv[1:]
        captured_output = io.StringIO()
        sys.stdout = captured_output
        my_count.main(args)
        sys.stdout = sys.__stdout__
        self.assertEqual("Number of words: 14, unique: 10; "
                         "average word length: 5.64 characters\n",
                         captured_output.getvalue())

    @patch('sys.argv', ['my_count/__init__.py', '-index'])
    def test_input_index(self):
        # wordcount -index
        # Enter text: Mary had a little lamb
        self.runTest(SAMPLE_TEXT,
                     "Number of words: 4, unique: 4; "
                     "average word length: 4.25 characters\n"
                     "Index:\n"
                     "had\n"
                     "lamb\n"
                     "little\n"
                     "Mary",
                     ['--index'])

    @patch('sys.argv', ['my_count/__init__.py',
                        '-index',
                        '-dictionary=dict.txt'])
    def test_input_index_and_dict(self):
        self.runTest(SAMPLE_TEXT,
                     "Number of words: 4, unique: 4; "
                     "average word length: 4.25 characters\n"
                     "Index (unknown: 2):\n"
                     "had\n"
                     "lamb*\n"
                     "little\n"
                     "Mary*",
                     ['--index', '--dictionary=dict.txt'])

    @patch('sys.argv', ['my_count/__init__.py',
                        '--index',
                        '--dictionary=dict.txt'])
    def test_input_index_and_dict_another(self):
        self.runTest("aaa",
                     "Number of words: 1, unique: 1; "
                     "average word length: 3.00 characters\n"
                     "Index (unknown: 1):\n"
                     "aaa*",
                     ['--index', '--dictionary=dict.txt'])

    @patch('sys.argv', ['wordcount.py'])
    def test_main_loop_with_simple(self):
        given_answer = SAMPLE_TEXT
        args = []
        expected_out = "Number of words: 4, unique: 4; " \
                       "average word length: 4.25 characters"
        with patch(BUILTINS_INPUT, return_value=given_answer), \
                patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
            wordcount.main(args)
            self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    @patch('sys.argv', ['wordcount.py'])
    def test_main_loop_without_args(self):
        given_answer = SAMPLE_TEXT
        args = ['--index']
        expected_out = HAD_LAMB_LITTLE_MARY_EXCEPTED_OUT
        with patch(BUILTINS_INPUT, return_value=given_answer), \
                patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
            wordcount.main(args)
            self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    @patch('sys.argv', ['wordcount.py'])
    def test_main_loop_without_valid_args(self):
        given_answer = " "
        args = ['-not_args', '-sssss']
        expected_out = HAD_LAMB_LITTLE_MARY_EXCEPTED_OUT
        with self.assertRaises(SystemExit):
            with patch(BUILTINS_INPUT, return_value=given_answer), \
                    patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
                wordcount.main(args)
                self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    def test_infinite_loop(self):
        wordcount.INFINITE_LOOP = False
        given_answer = " "
        args = ['-not_args', '-sssss']
        expected_out = HAD_LAMB_LITTLE_MARY_EXCEPTED_OUT
        with self.assertRaises(SystemExit):
            with patch(BUILTINS_INPUT, return_value=given_answer), \
                    patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
                wordcount.main(args)
                self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    def test_another_infinite_false_loop(self):
        wordcount.INFINITE_LOOP = False
        given_answer = ""
        args = []
        expected_out = ""
        with self.assertRaises(SystemExit):
            with patch(BUILTINS_INPUT, return_value=given_answer), \
                    patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
                wordcount.main(args)
                self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    def test_another_infinite_true_loop(self):
        wordcount.INFINITE_LOOP = True
        given_answer = ""
        args = []
        expected_out = ""
        with self.assertRaises(SystemExit):
            with patch(BUILTINS_INPUT, return_value=given_answer), \
                    patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
                wordcount.main(args)
                self.assertEqual(dummy_out.getvalue().strip(), expected_out)

    def test_second_infinite_true_loop(self):
        wordcount.INFINITE_LOOP = True
        given_answer = " "
        args = []
        expected_out = "Number of words: 0, unique: 0; " \
                       "average word length: 0.00 characters"
        with patch(BUILTINS_INPUT, return_value=given_answer), \
                patch(SYS_STDOUT, new=io.StringIO()) as dummy_out:
            wordcount.main(args)
            self.assertEqual(expected_out, dummy_out.getvalue().strip())

    def test_third_infinite_false_loop(self):
        given_answer = " "
        args = []

        with self.assertRaises(BaseException):
            self.call_infinite_main_loop(args, given_answer)

    @timeout(10)
    def call_infinite_main_loop(self, args, given_answer):
        with patch(BUILTINS_INPUT, return_value=given_answer), \
                patch(SYS_STDOUT, new=io.StringIO()):
            my_count.INFINITE_LOOP = False
            wordcount.main(args)

    def test_timeout(self):
        timeout(1)

    def test_timeout_helper(self):
        timeout(-10)
